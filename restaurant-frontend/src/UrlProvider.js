export const baseUrl = "localhost:22371/api/"
export const restaurantsUrl = baseUrl + "restaurants/"
export const menusUrl = baseUrl + "menus/"
export const menuItemsUrl = baseUrl + "menu_items/"
