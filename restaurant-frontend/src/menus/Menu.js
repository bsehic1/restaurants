import React from 'react'
import { Row, Col, Paper, Table } from '../Stateless'

class MenuItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <Col>
                <Row start>
                    <Col marginLeft={20}><h1>Restaurant Name</h1></Col>
                </Row>
                <Row>
                    <Paper padding={20} width={90}>
                        <Row>
                            <Paper>
                                <Row start>
                                    <h5>MenuName</h5>
                                    <p>Menu1</p>
                                </Row>
                                <Row start>
                                    <h5>Type</h5>
                                    <p>Breakfast</p>
                                </Row>
                            </Paper>
                            <Paper>
                                Menu Items
                            </Paper>
                        </Row>
                    </Paper>
                </Row>
            </Col>

        );
    }
}

export default MenuItem;