import React from 'react'
import './Restaurants.css'
import { TextField, Button, Checkbox, FormControlLabel } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Row, Col, Paper, Table } from '../Stateless'

const MaterialTextField = withStyles(theme => ({
    root: {
        width: '80%',
        marginBottom: '10%'
    },
}))(TextField);

const MaterialButton = withStyles(theme => ({
    root: {
        width: '130px',
        height: '55px',
        marginLeft: '20px'
    }
}))(Button)

const baseUrl = "http://localhost:22371/api/"
const restaurantsUrl = "http://localhost:22371/api/restaurants/"
const restaurantDeleteUrl = "http://localhost:22371/api/restaurant/"


class Restaurants extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurants: [],
            breakfast: false,
            brunch: false,
            lunch: false,
            dinner: false,
            italian: false,
            chinese: false,
            greek: false,
            local: false,
            vegetarian: false,
            takeout: false,
            online_reservation: false,
            queryString: ""
        }
    }

    componentDidMount() {
        this.getAllRestaurants();
    }

    getAllRestaurants = () => {
        fetch(restaurantsUrl)
            .then((response) => response.json())
            .then(json => {
                console.log(json);
                this.setState(state => {
                    state.restaurants = json;
                })
                this.setState({ state: this.state })
            });
    }

    handleClick = (_id) => {
        this.props.history.push({
            pathname: '/restaurant/',
            state: {
                id: _id
            }
        })
    }

    handleEdit = (_id, e) => {
        e.stopPropagation();
        this.props.history.push({
            pathname: '/restaurants/save',
            state: {
                id: _id
            }
        });
    }

    handleDelete = (id, e) => {
        e.stopPropagation();
        fetch(restaurantDeleteUrl + id, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        })
        .then(response=>response.json())
        .then(json=>{
            console.log(json);
            this.getAllRestaurants();
        });

    }

    handleChange = (name) => event => {
        let value = event.target.value;
        let checked = event.target.checked;
        this.setState((state) => {
            switch (name) {
                case 'breakfast':
                    state.breakfast = checked;
                    break;
                case 'brunch':
                    state.brunch = checked;
                    break;
                case 'lunch':
                    state.lunch = checked;
                    break;
                case 'dinner':
                    state.dinner = checked;
                    break;
                case 'italian':
                    state.italian = checked;
                    break;
                case 'chinese':
                    state.chinese = checked;
                    break;
                case 'greek':
                    state.greek = checked;
                    break;
                case 'local':
                    state.local = checked;
                    break;
                case 'vegetarian':
                    state.vegetarian = checked;
                    break;
                case 'takeout':
                    state.takeout = checked;
                    break;
                case 'online_reservation':
                    state.online_reservation = checked;
                    break;
                case 'query':
                    state.queryString = value;
                    break;
                default:
                    break;
            }
        });
        this.setState({ state: this.state });
    }

    handleAdd = () => {
        this.props.history.push({
            pathname: '/restaurants/save'
        });
    }

    handleQuery = () => {
        let queryUrl = restaurantsUrl + "?";
        queryUrl += "query=" + this.state.queryString;
        queryUrl += "&breakfast=" + this.state.breakfast;
        queryUrl += "&brunch=" + this.state.brunch;
        queryUrl += "&lunch=" + this.state.lunch;
        queryUrl += "&dinner=" + this.state.dinner;
        queryUrl += "&italian=" + this.state.italian;
        queryUrl += "&chinese=" + this.state.chinese;
        queryUrl += "&greek=" + this.state.greek;
        queryUrl += "&local=" + this.state.local;
        queryUrl += "&vegetarian=" + this.state.vegetarian;
        queryUrl += "&takeout=" + this.state.takeout;
        queryUrl += "&online_reservation=" + this.state.online_reservation;

        console.log(queryUrl);
        fetch(queryUrl)
            .then((response) => response.json())
            .then(json => {
                console.log(json);
                this.setState(state => {
                    state.restaurants = json;
                })
                this.setState({ state: this.state })
            });
    }


    render() {
        return (
            <Col>
                <Row start="true">
                    <Col marginLeft={4}><h1>Restaurants</h1></Col>
                </Row>
                <Row>
                    <Paper padding={20} width={90}>
                        <Row height={16} width={30} marginBottom={2}>
                            <MaterialTextField required variant="outlined" placeholder="Tai Chi Foods" label="Restaurant Name" onChange={this.handleChange("query")} />
                            <MaterialButton variant="contained" color="primary" size="large" onClick={this.handleQuery} >Search</MaterialButton>

                        </Row>
                        <Row marginBottom={2}>
                            <FormControlLabel
                                control={<Checkbox checked={this.state.breakfast} onChange={this.handleChange("breakfast")} />
                                }
                                label="breakfast"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.brunch} onChange={this.handleChange("brunch")} />
                                }
                                label="brunch"
                            />

                            <FormControlLabel
                                control={<Checkbox checked={this.state.lunch} onChange={this.handleChange("lunch")} />
                                }
                                label="lunch"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.dinner} onChange={this.handleChange("dinner")} />
                                }
                                label="dinner"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.italian} onChange={this.handleChange("italian")} />
                                }
                                label="italian"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.chinese} onChange={this.handleChange("chinese")} />
                                }
                                label="chinese"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.greek} onChange={this.handleChange("greek")} />
                                }
                                label="greek"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.local} onChange={this.handleChange("local")} />
                                }
                                label="local"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.vegetarian} onChange={this.handleChange("vegetarian")} />
                                }
                                label="vegetarian"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.takeout} onChange={this.handleChange("takeout")} />
                                }
                                label="takeout"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={this.state.online_reservation} onChange={this.handleChange("online_reservation")} />
                                }
                                label="online reservation"
                            />
                        </Row>
                        <Table>
                            <thead>
                                <tr>
                                    <th><h3>Name</h3></th>
                                    <th><h3>Address</h3></th>
                                    <th><h3>Description</h3></th>
                                    <th><h3>Actions</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.restaurants.map((item) =>
                                    <tr key={item._id} onClick={() => { this.handleClick(item._id) }}>
                                        <td>
                                            {item.name}
                                        </td>
                                        <td>
                                            {item.address}
                                        </td>
                                        <td>
                                            {item.description}
                                        </td>
                                        <td width={400}>
                                            <Row>
                                                <MaterialButton variant="contained" color="primary" size="large" onClick={(e) => this.handleEdit(item._id, e)}>Edit</MaterialButton>
                                                <MaterialButton variant="contained" color="secondary" size="large" onClick={(e) => this.handleDelete(item._id, e)}>Delete</MaterialButton>
                                            </Row>
                                        </td>
                                    </tr>
                                )}

                            </tbody>
                        </Table>

                    </Paper>
                </Row>
                <Row end="true" marginBottom={8}>
                    <Col marginRight={4}>
                        <Button variant="contained" color="primary" size="large" onClick={this.handleAdd}>Add</Button>
                    </Col>
                </Row>
            </Col>
        );
    }
}

export default Restaurants;