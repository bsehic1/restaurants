import React from 'react'
import { Row, Col, Paper } from '../Stateless'
import { Checkbox, Divider, Button,FormControlLabel } from '@material-ui/core';

const restaurantsUrl = "http://localhost:22371/api/restaurants/"

class Restaurant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state.id
        }
    }

    componentDidMount() {
        if (this.state.id) {

            fetch(restaurantsUrl + this.state.id)
                .then(respone => respone.json())
                .then(json => {
                    if (json[0] != null) {
                        this.setState(state => {
                            state.restaurant = json[0]
                        });
                        this.setState({ state: this.state });
                    }
                })
        }
    }

    handleBack = () => {
        this.props.history.push({
            pathname: '/restaurants/'
        });
    }

    render() {
        return (
            <Col>
                <Row start>
                    <Col marginLeft={4}><h1>{this.state.restaurant != null ? this.state.restaurant.name : ""}</h1></Col>
                </Row>
                <Row>
                    <Paper padding={40} width={90}>
                        <Row>
                            <Paper width={30} padding={20} height={60} top>
                                <div>
                                    <h3>Name</h3>
                                    <p>{this.state.restaurant != null ? this.state.restaurant.name : ""}</p>
                                </div>
                                <div>
                                    <h3>Address</h3>
                                    <p>{this.state.restaurant != null ? this.state.restaurant.address : ""}</p>
                                </div>
                                <div>
                                    <h3>Description</h3>
                                    <p>{this.state.restaurant != null ? this.state.restaurant.description : ""}</p>
                                </div>
                            </Paper>
                            <Paper width={24}>
                                <Row>
                                    <h5>Meals</h5>
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.breakfast : false} />
                                        }
                                        label="breakfast"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.brunch : false} />
                                        }
                                        label="brunch"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.lunch : false} />
                                        }
                                        label="lunch"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.dinner : false} />
                                        }
                                        label="dinner"
                                    />
                                </Row>
                                <Divider></Divider>
                                <Row><h5>Cuisines</h5></Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.italian : false} />
                                        }
                                        label="italian"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.chinese : false} />
                                        }
                                        label="chinese"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.greek : false} />
                                        }
                                        label="greek"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.local : false} />
                                        }
                                        label="local"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.vegetarian : false} />
                                        }
                                        label="vegetarian"
                                    />
                                </Row>
                            </Paper>
                            <Paper width={24} top height={30}>
                                <Row><h5>Options</h5></Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.takeout : false} />
                                        }
                                        label="takeout"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox disabled checked={this.state.restaurant != null ? this.state.restaurant.online_reservation : false} />
                                        }
                                        label="online reservation"
                                    />
                                </Row>
                            </Paper>
                        </Row>
                    </Paper>
                </Row>
                <Row end marginBottom={8}>
                    <Col marginRight={1}>
                        <Button variant="contained" color="secondary" size="large" onClick={this.handleBack}>Back to Restaurants</Button>
                    </Col>
                </Row>
            </Col>

        );
    }
}

export default Restaurant;