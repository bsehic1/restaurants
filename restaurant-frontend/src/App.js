import React from 'react'
import Restaurants from './restaurants/Restaurants'
import Restaurant from './restaurants/Restaurant'
import RestaurantSave from './restaurants/RestaurantSave'
import Menu from './menus/Menu'
import Menus from './menus/Menus'
import MenuSave from './menus/MenuSave'
import MenuItem from './menuItems/MenuItem'
import MenuItems from './menuItems/MenuItems'
import MenuItemSave from './menuItems/MenuItemSave'

import { BrowserRouter as Router,Route } from 'react-router-dom'
import { SnackbarProvider } from 'notistack'

import './App.css';

function App() {
  return (
    <SnackbarProvider maxSnack={5}>
      <Router>
        <div style={{ minHeight: '100vh', backgroundImage: 'linear-gradient(45deg, rgba(245,245,245,1) 30%, rgba(250,250,250,1) 30%)' }}>
          <Route exact path="/" component={Restaurants}/>
          <Route exact path="/restaurants/" component={Restaurants} />
          <Route path="/restaurant/" component={Restaurant} />
          <Route path="/restaurants/save/" component={RestaurantSave} />
        </div>
      </Router>
    </SnackbarProvider>
  );
}

export default App;
