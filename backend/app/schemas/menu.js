const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const MenuItem = require('./menuItem');

const MenuSchema = new Schema({
    name: String,
    description: String,
    restaurant_id: {type: ObjectId, ref:"Restaurant"}
});

MenuSchema.pre("remove",(next)=>{
    MenuItem.remove({menu_id: this._id}).exec();
    next();
});

module.exports = mongoose.model("Menu", MenuSchema);
