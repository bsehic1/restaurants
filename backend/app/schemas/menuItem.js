const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;


const MenuItemSchema = new Schema({
    name: String,
    price: Number,
    menu_id: { type: ObjectId, ref: "Menu" },
    Ingredients: String
});

module.exports = mongoose.model("MenuItem", MenuItemSchema);