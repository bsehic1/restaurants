import React from 'react';
import ReactDOM from 'react-dom';
import MenuItemSave from './MenuItemSave';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MenuItemSave />, div);
  ReactDOM.unmountComponentAtNode(div);
});
