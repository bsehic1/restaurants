import React from 'react'
import { Row, Col, Paper, Table } from '../Stateless'

class Menus extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurants: [
                {
                    name: "Neko ime",
                    address: "Zagrebacka 123",
                    contact_number: "+38733547852",
                    website: "www.nekiweb.com",
                },
                {
                    name: "Neko ime",
                    address: "Zagrebacka 123",
                    contact_number: "+38733547852",
                    website: "www.nekiweb.com",
                },
                {
                    name: "Neko ime",
                    address: "Zagrebacka 123",
                    contact_number: "+38733547852",
                    website: "www.nekiweb.com",
                },
                {
                    name: "Neko ime",
                    address: "Zagrebacka 123",
                    contact_number: "+38733547852",
                    website: "www.nekiweb.com",
                },
                {
                    name: "Neko ime",
                    address: "Zagrebacka 123",
                    contact_number: "+38733547852",
                    website: "www.nekiweb.com",
                },
                {
                    name: "Neko ime",
                    address: "Zagrebacka 123",
                    contact_number: "+38733547852",
                    website: "www.nekiweb.com",
                },
                {
                    name: "Neko ime",
                    address: "Zagrebacka 123",
                    contact_number: "+38733547852",
                    website: "www.nekiweb.com",
                }
            ]
        }
    }

    render() {
        return (
            <Col>
                <Row start>
                    <Col marginLeft={20}><h1>Menus</h1></Col>
                </Row>
                <Row>
                    <Paper padding={20} width={90}>
                        <Table>
                            <tr>
                                <th><h3>Name</h3></th>
                                <th><h3>Address</h3></th>
                                <th><h3>Contact Number</h3></th>
                                <th><h3>Website</h3></th>
                                <th><h3>Action</h3></th>
                            </tr>
                            {this.state.restaurants.map((item)=>
                                <tr>
                                    <td>
                                        {item.name}
                                    </td>
                                    <td>
                                        {item.address}
                                    </td>
                                    <td>
                                        {item.contact_number}
                                    </td>
                                    <td>
                                        {item.website}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            )}
                        </Table>
                    </Paper>
                </Row>
            </Col>

        );
    }
}

export default Menus;