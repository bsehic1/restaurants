import styled, { css } from 'styled-components'

export const Row = styled.div`
    display:flex;
    justify-content:space-evenly;
    ${props => props.container && css`
        height:100%;
        width:100%;
    `}
    ${props => props.fill && css`
        height:100%;
    `}
    ${props => props.start && css`
        justify-content:flex-start;
    `}
    ${props => props.center && css`
        justify-content:center;
    `}
    ${props => props.end && css`
        justify-content:flex-end;
    `}
    margin-top: ${props => props.marginTop + "%;" || "0%;"}
    margin-bottom: ${props => props.marginBottom + "%;" || "0%;"}
    margin-left: ${props => props.marginLeft + "%;" || "0%;"}
    margin-right: ${props => props.marginRight + "%;" || "0%;"}
    padding-top: ${props => props.paddingTop + "%;" || "0%;"}
    padding-bottom: ${props => props.paddingBottom + "%;" || "0%;"}
    padding-left: ${props => props.paddingLeft + "%;" || "0%;"}
    padding-right: ${props => props.paddingRight + "%;" || "0%;"}
    width: ${props => props.width + "%;"}
    height: ${props => props.height + "%;" || "0%;"}
`
export const Col = styled.div`
    display:flex;
    flex-direction:column;
    ${props => props.fill && css`
        width:100%;
        height:100%;
    `}
    ${props => props.spaceEvenly && css`
        justify-content:space-evenly;
    `}
    ${props => props.start && css`
        justify-content:flex-start;
    `}
    ${props => props.center && css`
        justify-content:center;
    `}
    ${props => props.end && css`
        justify-content:flex-end;
    `}
    ${props => props.outer && css`
        background-image: linear-gradient(45deg, rgba(245,245,245,1) 30%, rgba(250,250,250,1) 30%);
    `}
    padding: ${props => props.padding + "%;" || "0%;"}
    margin-top: ${props => props.marginTop + "%;" || "0%;"}
    margin-bottom: ${props => props.marginBottom + "%;" || "0%;"}
    margin-left: ${props => props.marginLeft + "%;" || "0%;"}
    margin-right: ${props => props.marginRight + "%;" || "0%;"}
    width: ${props => props.width + "%;"}
    height: ${props => props.height + "%;" || "0%;"}
`
export const Paper = styled.div`
    display:flex;
    flex-direction:column;
    justify-content:space-evenly;
    box-shadow: 1px 2px 4px 0px #bdbdbd;
    border-radius: 4px;
    background-color:white;
    margin-bottom:40px;
    padding-left: ${props => props.paddingLeft + "px;" || "0px;"}
    padding-right: ${props => props.paddingRight + "px;" || "0px;"}
    padding-top: ${props => props.paddingTop + "px;" || "0px;"}
    padding-bottom: ${props => props.paddingBottom + "px;" || "0px;"}
    padding: ${props => props.padding + "px;" || "0px;"}
    margin: ${props => props.margin + "px;" || "0px;"}
    width: ${props => props.width + "%;" || "0%;"}
    height: ${props => props.height + "%;" || "0%;"}
    ${props => props.fill && css`
        width:100%;
        height:100%;
    `}
    ${props => props.top && css`
        justify-content:flex-start;
    `}
    ${props => props.center && css`
        justify-content:center;
    `}
    ${props => props.bottom && css`
        justify-content:flex-end;
    `}
`

export const Table = styled.table`
    background-color:white;
    max-height: 60%;
    padding-left: ${props => props.paddingLeft + "px;" || "0px;"}
    padding-right: ${props => props.paddingRight + "px;" || "0px;"}
    padding-top: ${props => props.paddingTop + "px;" || "0px;"}
    padding-bottom: ${props => props.paddingBottom + "px;" || "0px;"}
    padding: ${props => props.padding + "px;" || "0px;"}
    margin: ${props => props.margin + "px;" || "0px;"}
    width: ${props => props.width + "px;" || "0px;"}
    height: ${props => props.height + "px;" || "0px;"}
`