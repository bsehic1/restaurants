const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const Menu = require('./menu');

const RestaurantSchema = new Schema({
    name: String,
    address: String,
    description: String,
    breakfast: Boolean,
    brunch: Boolean,
    lunch: Boolean,
    dinner: Boolean,
    italian: Boolean,
    chinese: Boolean,
    greek: Boolean,
    local: Boolean,
    vegetarian: Boolean,
    takeout: Boolean,
    online_reservation: Boolean
});
RestaurantSchema.pre("remove",(next)=>{
    Menu.remove({restaurant_id: this._id}).exec();
    next();
});

module.exports = mongoose.model("Restaurant", RestaurantSchema);