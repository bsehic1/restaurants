import React from 'react'
import { Row, Col, Paper } from '../Stateless'
import { withStyles } from '@material-ui/core/styles'
import { TextField, Checkbox, Divider, Button } from '@material-ui/core'

const MaterialTextField = withStyles(theme => ({
    root: {
        width: '80%',
        marginTop: '10%',
        marginBottom: '10%'
    },
}))(TextField);


class MenuItemSave extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <Col>
                <Row start>
                    <Col marginLeft={6}><h1>Create new Menu Item</h1></Col>
                </Row>
                <Row>
                    <Paper padding={20} width={90}>
                        <Row marginTop={2}>
                            <Paper width={30} height={30} top>
                                <Row>
                                    <MaterialTextField required variant="outlined" placeholder="Fusili" label="Menu Item Name" />
                                </Row>
                                <Row>
                                    <MaterialTextField variant="outlined" placeholder="$20.00" label="Price" />
                                </Row>
                                <Row>
                                    <MaterialTextField multiline rows={10} variant="outlined" placeholder="Veal, Paprika, Sauce" label="Ingredients" />
                                </Row>
                            </Paper>
                        </Row>
                    </Paper>
                </Row>
                <Row end marginBottom={8}>
                    <Col marginRight={1}>
                        <Button variant="contained" color="secondary" size="large">Discard</Button>
                    </Col>
                    <Col marginRight={4}>
                        <Button variant="contained" color="primary" size="large">Save</Button>
                    </Col>
                </Row>
            </Col>

        );
    }
}

export default MenuItemSave;