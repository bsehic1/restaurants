const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const Restaurant = require("./app/schemas/restaurant");
const Menu = require('./app/schemas/menu');
const MenuItem = require('./app/schemas/menuItem');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const port = process.env.PORT || 22371;

// db
const mongoose = require('mongoose');
mongoose.connect('mongodb://root:root@localhost:27017/restaurant_db?authSource=admin', { useNewUrlParser: true });


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
});

const router = express.Router();

// middleware to use for all requests
router.use((req, res, next) => {
  // do logging
  console.log('Something is happening.');
  next(); // make sure we go to the next routes and don't stop here
});

router.get("/", (req, res) => {

  // let icaffe = new Restaurant({name: "iCaffe"});

  // icaffe.save((err,item)=>{
  //   if(err){
  //     return console.error(err);
  //   }
  //   res.json(item);
  // })
});


//restaurant
router.route("/restaurants/")
  .post((req, res) => {
    let restaurant = new Restaurant();
    req.body.name != null ? restaurant.name = req.body.name : null;
    req.body.address != null ? restaurant.address = req.body.address : null;
    req.body.description != null ? restaurant.description = req.body.description : null;
    req.body.breakfast != null ? restaurant.breakfast = req.body.breakfast : null;
    req.body.brunch != null ? restaurant.brunch = req.body.brunch : null;
    req.body.lunch != null ? restaurant.lunch = req.body.lunch : null;
    req.body.dinner != null ? restaurant.dinner = req.body.dinner : null;
    req.body.italian != null ? restaurant.italian = req.body.italian : null;
    req.body.chinese != null ? restaurant.chinese = req.body.chinese : null;
    req.body.greek != null ? restaurant.greek = req.body.greek : null;
    req.body.local != null ? restaurant.local = req.body.local : null;
    req.body.vegetarian != null ? restaurant.vegetarian = req.body.vegetarian : null;
    req.body.takeout != null ? restaurant.takeout = req.body.takeout : null;
    req.body.online_reservation != null ? restaurant.online_reservation = req.body.online_reservation : null;

    restaurant.save((err) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json({ message: "Restaurant created!" });
    });
  });

router.route("/restaurants")
  .get((req, res) => {
    let queryObject = {
      $and: []
    };

    if (req.query.query != null && req.query.query != undefined && req.query.query != "") {
      queryObject.$and.push({
        $or: [
          { name: { $regex: req.query.query,  $options: 'i' }},
          { address: { $regex: req.query.query,  $options: 'i' }},
          { description: { $regex: req.query.query,  $options: 'i' }}
        ]
      });
    }
    req.query.breakfast == "true" ? queryObject.$and.push({ breakfast: req.query.breakfast }) : null;
    req.query.brunch == "true" ? queryObject.$and.push({ brunch: req.query.brunch}) : null;
    req.query.lunch == "true" ? queryObject.$and.push({ lunch: req.query.lunch }) : null;
    req.query.dinner == "true" ? queryObject.$and.push({ dinner: req.query.dinner }) : null;
    req.query.italian == "true" ? queryObject.$and.push({ italian: req.query.italian }) : null;
    req.query.chinese == "true" ? queryObject.$and.push({ chinese: req.query.chinese }) : null;
    req.query.greek == "true" ? queryObject.$and.push({ greek: req.query.greek }) : null;
    req.query.local == "true" ? queryObject.$and.push({ local: req.query.local }) : null;
    req.query.vegetarian == "true" ? queryObject.$and.push({ vegetarian: req.query.vegetarian }) : null;
    req.query.takeout == "true" ? queryObject.$and.push({ takeout: req.query.takeout }) : null;
    req.query.online_reservation == "true" ? queryObject.$and.push({ online_reservation: req.query.online_reservation }) : null;

    if(queryObject.$and.length == 0){
      queryObject = {};
    }

    Restaurant.find(queryObject, (err, items) => {

      if (err) {
        res.send(err);
        return;
      }
      res.json(items);
    })
  });

router.route("/restaurants/:id")
  .get((req, res) => {
    Restaurant.find({ _id: req.params.id }, (err, item) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json(item);
    });
  })
  .put((req, res) => {
    Restaurant.find({ _id: req.params.id }, (err, items) => {
      if (err) {
        res.send(err);
        return;
      }
      let restaurant = items[0];
      if (restaurant) {
        req.body.name != null ? restaurant.name = req.body.name : null;
        req.body.address != null ? restaurant.address = req.body.address : null;
        req.body.description != null ? restaurant.description = req.body.description : null;
        req.body.breakfast != null ? restaurant.breakfast = req.body.breakfast : null;
        req.body.brunch != null ? restaurant.brunch = req.body.brunch : null;
        req.body.lunch != null ? restaurant.lunch = req.body.lunch : null;
        req.body.dinner != null ? restaurant.dinner = req.body.dinner : null;
        req.body.italian != null ? restaurant.italian = req.body.italian : null;
        req.body.chinese != null ? restaurant.chinese = req.body.chinese : null;
        req.body.greek != null ? restaurant.greek = req.body.greek : null;
        req.body.local != null ? restaurant.local = req.body.local : null;
        req.body.vegetarian != null ? restaurant.vegetarian = req.body.vegetarian : null;
        req.body.takeout != null ? restaurant.takeout = req.body.takeout : null;
        req.body.online_reservation != null ? restaurant.online_reservation = req.body.online_reservation : null;

        restaurant.save((err) => {
          if (err) {
            res.send(err);
            return;
          }
          res.json({ message: "Restaurant updated!" });
        });
      }

    });
  })
  .delete((req, res) => {
    Restaurant.remove({ _id: req.params.id }, (err, item) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json({ message: "Restaurant deleted!" });
    });
  });


//menu
router.route("/menus")
  .post((req, res) => {
    let menu = new Menu();
    req.body.name != null ? menu.name = req.body.name : null;
    req.body.description != null ? menu.description = req.body.description : null;
    req.body.restaurant_id != null ? menu.restaurant_id = req.body.restaurant_id : null;

    menu.save((err) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json({ message: "Menu created!" })
    });
  })
  .get((req, res) => {
    Menu.find((err, items) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json(items);
    })
  });

router.route("/menus/:id")
  .get((req, res) => {
    Menu.find({ _id: req.params.id }, (err, items) => {
      if (err) {
        res.send(err);
      }
      res.json(items);
    });
  })
  .put((req, res) => {
    Menu.find({ _id: req.params.id }, (err, items) => {
      if (err) {
        res.send(err);
        return;
      }
      let menu = items[0];
      if (menu) {
        req.body.name != null ? menu.name = req.body.name : null;
        req.body.description != null ? menu.description = req.body.description : null;
        req.body.restaurant_id != null ? menu.restaurant_id = req.body.restaurant_id : null;

        menu.save((err) => {
          if (err) {
            res.send(err);
            return;
          }
          res.json({ message: "Menu updated!" });
        });
      }

    })
  })
  .delete((req, res) => {
    Menu.remove({ _id: req.params.id }, (err, item) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json({ message: "Menu deleted!" });
    });
  });

//menuItem
router.route("/menu_items")
  .post((req, res) => {
    let menuItem = new MenuItem();
    req.body.name != null ? menuItem.name = req.body.name : null;
    req.body.price != null && !isNaN(req.body.price) ? menuItem.price = req.body.price : null;
    req.body.ingredients != null ? menuItem.ingredients = req.body.ingredients : null;
    req.body.menu_id != null ? menuItem.menu_id = req.body.menu_id : null;

    menuItem.save((err) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json({ message: "MenuItem created!" })
    });
  })
  .get((req, res) => {
    MenuItem.find((err, items) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json(items);
    })
  });

router.route("/menus_items/:id")
  .get((req, res) => {
    MenuItem.find({ _id: req.params.id }, (err, item) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json(item);
    });
  })
  .put((req, res) => {
    MenuItem.find({ _id: req.params.id }, (err, items) => {
      if (err) {
        res.send(err);
        return;
      }
      let menuItem = items[0];
      if (menuItem) {
        req.body.name != null ? menuItem.name = req.body.name : null;
        req.body.price != null && !isNaN(req.body.price) ? menuItem.price = parseFloat(req.body.price) : null;
        req.body.ingredients != null ? menuItem.ingredients = req.body.ingredients : null;
        req.body.menu_id != null ? menuItem.menu_id = req.body.menu_id : null;

        menuItem.save((err) => {
          if (err) {
            res.send(err);
            return;
          }
          res.json({ message: "Menu item updated!" });
        });
      }
    })
  })
  .delete((req, res) => {
    MenuItem.remove({ _id: req.params.id }, (err, item) => {
      if (err) {
        res.send(err);
        return;
      }
      res.json({ message: "MenuItem deleted!" });
    });
  });


app.use('/api', router);

//starting server
app.listen(port);
console.log('Magic happenes on port: ' + port);

// some errorHandler i found online
errorHandler = (err, req, res, next) => {
  console.error(err.stack)
  res.status(500)
  res.render('error', { error: err })
  next();
}

