import React from 'react'
import { Row, Col, Paper } from '../Stateless'
import { withStyles } from '@material-ui/core/styles'
import { TextField, Checkbox, Divider, Button, FormControlLabel } from '@material-ui/core'

const MaterialTextField = withStyles(theme => ({
    root: {
        width: '80%',
        marginTop: '10%',
        marginBottom: '10%'
    },
}))(TextField);

const restaurantsUrl = "http://localhost:22371/api/restaurants/"

class RestaurantSave extends React.Component {
    constructor(props) {
        super(props);
        let id;
        if (this.props.location.state == null) {
            id = null;
        } else {
            id = this.props.location.state.id;
        } 
        this.state = {
            id: id,
            name: null,
            address: null,
            description: null,
            breakfast: false,
            brunch: false,
            lunch: false,
            dinner: false,
            italian: false,
            chinese: false,
            greek: false,
            local: false,
            vegetarian: false,
            takeout: false,
            online_reservation: false
        }
    }

    componentDidMount() {
        if (this.state.id) {
            fetch(restaurantsUrl + this.state.id)
                .then(response => response.json())
                .then(json => {
                    if (json) {
                        console.log(json);
                        this.setState(state => {
                            state.name = json[0].name != null ? json[0].name : "";
                            state.address = json[0].address != null ? json[0].address : "";
                            state.description = json[0].description != null ? json[0].description : "";
                            state.breakfast = json[0].breakfast != null ? json[0].breakfast : false;
                            state.brunch = json[0].brunch != null ? json[0].brunch : false;
                            state.lunch = json[0].lunch != null ? json[0].lunch : false;
                            state.dinner = json[0].dinner != null ? json[0].dinner : false;
                            state.italian = json[0].italian != null ? json[0].italian : false;
                            state.chinese = json[0].chinese != null ? json[0].chinese : false;
                            state.greek = json[0].greek != null ? json[0].greek : false;
                            state.local = json[0].local != null ? json[0].local : false;
                            state.vegetarian = json[0].vegetarian != null ? json[0].vegetarian : false;
                            state.takeout = json[0].takeout != null ? json[0].takeout : false;
                            state.online_reservation = json[0].online_reservation != null ? json[0].online_reservation : false;
                        });
                    }
                    this.setState({ state: this.state });
                });
        }

    }

    handleChange = (name) => event => {
        let value = event.target.value;
        let checked = event.target.checked;
        this.setState((state) => {
            switch (name) {
                case 'name':
                    state.name = value;
                    break;
                case 'address':
                    state.address = value;
                    break;
                case 'description':
                    state.description = value;
                    break;
                case 'breakfast':
                    state.breakfast = checked;
                    break;
                case 'brunch':
                    state.brunch = checked;
                    break;
                case 'lunch':
                    state.lunch = checked;
                    break;
                case 'dinner':
                    state.dinner = checked;
                    break;
                case 'italian':
                    state.italian = checked;
                    break;
                case 'chinese':
                    state.chinese = checked;
                    break;
                case 'greek':
                    state.greek = checked;
                    break;
                case 'local':
                    state.local = checked;
                    break;
                case 'vegetarian':
                    state.vegetarian = checked;
                    break;
                case 'takeout':
                    state.takeout = checked;
                    break;
                case 'online_reservation':
                    state.online_reservation = checked;
                    break;
                case 'queryString':
                    state.queryString = value;
                    break;
                default:
                    break;
            }
        });
        this.setState({ state: this.state });
    }

    handleSave = () => {

        let restaurant = {
            name: this.state.name,
            address: this.state.address,
            description: this.state.description,
            breakfast: this.state.breakfast,
            brunch: this.state.brunch,
            lunch: this.state.lunch,
            dinner: this.state.dinner,
            italian: this.state.italian,
            chinese: this.state.chinese,
            greek: this.state.greek,
            local: this.state.local,
            vegetarian: this.state.vegetarian,
            takeout: this.state.takeout,
            online_reservation: this.state.online_reservation
        }

        if (this.state.id != null) {
            fetch(restaurantsUrl + this.state.id, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                },
                mode: 'cors',
                body: JSON.stringify(restaurant)
            })
                .then(response=> {
                    console.log(response.json());
                    this.props.history.push({
                        pathname: '/restaurants/'
                    });
                })
                .catch((err) => console.log(err));
            return;
        }

        fetch(restaurantsUrl, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            mode: 'cors',
            body: JSON.stringify(restaurant)
        })
            .then((response) => {
                console.log(response.json())
                this.props.history.push({
                    pathname: '/restaurants/'
                });
            })
            .catch((err) => console.log(err));
    }

    handleDiscard = () => {
        this.props.history.push({
            pathname: '/restaurants/'
        });
    }


    render() {
        return (
            <Col>
                <Row start>
                    <Col marginLeft={4}><h1>{this.state.name != null && this.state.name != "" ? this.state.name : "Create new Restaurant"}</h1></Col>
                </Row>
                <Row>
                    <Paper width={90}>
                        <Row marginTop={2}>
                            <Paper width={30} height={30} top>
                                <Row>
                                    <MaterialTextField required variant="outlined" value={this.state.name} label="Restaurant Name" onChange={this.handleChange("name")} />
                                </Row>
                                <Row>
                                    <MaterialTextField variant="outlined" value={this.state.address} label="Address" onChange={this.handleChange("address")} />
                                </Row>
                                <Row>
                                    <MaterialTextField multiline rows={10} value={this.state.description} variant="outlined" label="Description" onChange={this.handleChange("description")} />
                                </Row>
                            </Paper>
                            <Paper width={24}>
                                <Row>
                                    <h5>Meals</h5>
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox checked={this.state.breakfast} onChange={this.handleChange("breakfast")} />
                                        }
                                        label="breakfast"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.brunch} onChange={this.handleChange("brunch")} />}
                                        label="brunch"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.lunch} onChange={this.handleChange("lunch")} />
                                        }
                                        label="lunch"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.dinner} onChange={this.handleChange("dinner")} />
                                        }
                                        label="dinner"
                                    />
                                </Row>
                                <Divider></Divider>
                                <Row><h5>Cuisines</h5></Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.italian} onChange={this.handleChange("italian")} />
                                        }
                                        label="italian"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.chinese} onChange={this.handleChange("chinese")} />
                                        }
                                        label="chinese"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.greek} onChange={this.handleChange("greek")} />
                                        }
                                        label="greek"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.local} onChange={this.handleChange("local")} />
                                        }
                                        label="local"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.vegetarian} onChange={this.handleChange("vegetarian")} />
                                        }
                                        label="vegetarian"
                                    />
                                </Row>
                            </Paper>
                            <Paper width={24} top height={30}>
                                <Row><h5>Options</h5></Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.takeout} onChange={this.handleChange("takeout")} />
                                        }
                                        label="takeout"
                                    />
                                </Row>
                                <Row start paddingLeft={30}>
                                    <FormControlLabel
                                        control={<Checkbox checked={this.state.online_reservation} onChange={this.handleChange("online_reservation")} />
                                        }
                                        label="online reservation"
                                    />
                                </Row>
                            </Paper>
                        </Row>
                    </Paper>
                </Row>
                <Row end marginBottom={8}>
                    <Col marginRight={1}>
                        <Button variant="contained" color="secondary" size="large" onClick={this.handleDiscard}>Discard</Button>
                    </Col>
                    <Col marginRight={4}>
                        <Button variant="contained" color="primary" size="large" onClick={this.handleSave}>Save</Button>
                    </Col>
                </Row>
            </Col>

        );
    }
}

export default RestaurantSave;