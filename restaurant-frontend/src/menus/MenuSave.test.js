import React from 'react';
import ReactDOM from 'react-dom';
import MenuSave from './MenuSave';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MenuSave />, div);
  ReactDOM.unmountComponentAtNode(div);
});
